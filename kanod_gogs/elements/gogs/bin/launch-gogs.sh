#!/bin/bash

#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


set -eu

IP_ADDRESS=${1}
GOGS_ADMIN_PASSWORD=${2}

if [ ! -d /data/pgsql-data ]
then
    echo "Creating PostgreSQL data directory"
    mkdir -p /data/pgsql-data
    chown postgres:postgres /data/pgsql-data/
    su postgres -c " /usr/lib/postgresql/10/bin/initdb -D /data/pgsql-data"
    systemctl enable postgresql
    systemctl restart postgresql
    su postgres -c "psql -c \"CREATE USER git WITH PASSWORD 'git' CREATEDB;\""
    su postgres -c "psql -c \"CREATE DATABASE gogs_db OWNER git;\""
    echo "Creating GOGS data directory"
    mkdir -p /data/gogs-data
    chown -R git:git /data/gogs-data
    systemctl restart gogs
    sleep 30
    systemctl stop gogs
    su git -c "/opt/gogs/gogs admin create-user --name \"gogsadmin\" --password \"${GOGS_ADMIN_PASSWORD}\" --admin --email gogsadmin@localhost"
fi

echo "Configuring external IP address"
sed -i "s|EXTERNAL_URL[ \t]*=.*$|EXTERNAL_URL = https://${IP_ADDRESS}|" /opt/gogs/custom/conf/app.ini

echo "Configuring nginx"
if ! [ -f /etc/nginx/certs/key.pem ]; then
    mkdir -p /etc/nginx/certs
    touch /root/.rnd && chmod 600 /root/.rnd
    openssl req -x509 -nodes -days 3650 -newkey rsa:2048 -keyout /etc/nginx/certs/key.pem -out /etc/nginx/certs/cert.pem -addext "subjectAltName = IP:${IP_ADDRESS}" -subj "/C=--/L=-/O=-/CN=${IP_ADDRESS}"
fi

echo "Configuring and starting ufw"
ufw --force enable
ufw allow 443/tcp
ufw allow 22/tcp
ufw status numbered

echo "Starting postgresql/gogs/nginx services"
systemctl enable postgresql
systemctl restart postgresql
systemctl enable gogs
systemctl start gogs
systemctl enable nginx
systemctl restart nginx
