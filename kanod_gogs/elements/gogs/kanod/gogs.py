#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


import os
from os import path
import subprocess
import requests
import time
import urllib


from kanod_configure import common


url = 'http://localhost:3000'


def declare_users(gogs, auth):
    users = gogs.get('users', [])
    usernames = []
    for user in users:
        username = user.get('name', None)
        password = user.get('password', None)
        if username is None or password is None:
            print('Incomplete entry while processing gogs user')
            continue
        usernames.append(username)
        email = user.get('email', f'{username}@localhost.localdomain')
        request = {'username': username, 'password': password, 'email': email}
        resp = requests.post(
            f'{url}/api/v1/admin/users',
            json=request, auth=auth)
        if resp.status_code not in [200, 201]:
            print(resp.content.decode('utf-8'))
    return usernames


def declare_team(usernames, org, orgname, role, perm, auth):
    members = org.get(f'{role}s', [])
    if len(members) > 0:
        request = {'name': f'{orgname}-{role}s', 'permission': perm}
        resp = requests.post(
            f'{url}/api/v1/admin/orgs/{orgname}/teams',
            json=request, auth=auth)
        if resp.status_code not in [200, 201]:
            print(f'Cannot create team of {role}s for {orgname}:'
                  f'{resp.status_code}')
            print(resp.content.decode('utf-8'))
            return
        team_id = resp.json().get('id', None)
        if team_id is None:
            print('Cannot find team id')
            return
        for member in members:
            if member not in usernames:
                print(f'Unknown {role} member of {orgname}: {member}')
                return
            resp = requests.put(
                f'{url}/api/v1/admin/teams/{team_id}/members/{member}',
                auth=auth
            )
            if resp.status_code != 204:
                print(f'Cannot add {member} as {role} of {orgname}')


def declare_organizations(gogs, usernames, auth):
    orgnames = []
    orgs = gogs.get('organizations', [])
    for org in orgs:
        orgname = org.get('name', None)
        orgnames.append(orgname)
        owner = org.get('owner', None)
        if orgname is None or owner is None:
            print('Incomplete entry while processing gogs organization')
            continue
        if owner not in usernames:
            print(f'unknown owner {owner} for organization {orgname}')
            continue
        description = org.get('description', f'organization {orgname}')
        request = {'username': orgname, 'description': description}
        resp = requests.post(
            f'{url}/api/v1/admin/users/{owner}/orgs',
            json=request, auth=auth)
        if resp.status_code not in [200, 201]:
            print(f'Cannot create organization {orgname}')
            print(resp.content.decode('utf-8'))
            continue
        for (role, perm) in [
            ('admin', 'admin'),
            ('writer', 'write'),
            ('reader', 'read')
        ]:
            declare_team(
                usernames=usernames, org=org, orgname=orgname, role=role,
                perm=perm, auth=auth)
    return orgnames


def create_token(auth):
    (user, _) = auth
    request = {'name': 'config'}
    resp = requests.post(
        f'{url}/api/v1/users/{user}/tokens',
        json=request,
        auth=auth)
    if resp.status_code == 201:
        return resp.json()['sha1']
    return None


def declare_project_webhook(owner, project_name, webhook, token):
    webhook_url = webhook.get('url', None)
    webhook_secret = webhook.get('secret', None)
    if webhook_url is None:
        return
    request = {
        'type': 'gogs',
        'config': {
            'url': webhook_url,
            'content_type': 'json'
        },
        'active': True
    }
    if webhook_secret is not None:
        request['config']['secret'] = webhook_secret
    resp = requests.post(
        f'{url}/api/v1/repos/{owner}/{project_name}/hooks',
        headers={'Authorization': f'token {token}'},
        json=request)
    if resp.status_code not in [200, 201]:
        print(resp.content.decode('utf-8'))


def declare_project(gogs, usernames, orgnames, auth):
    projects = gogs.get('projects', [])
    token = create_token(auth)
    for project in projects:
        name = project.get('name', None)
        owner = project.get('owner', None)
        if name is None or owner is None:
            print('Incomplete entry while processing gogs project')
            continue
        if owner not in usernames and owner not in orgnames:
            print(f'Unknown owner {owner} for project {name}')
            continue
        description = project.get('description', f'project {name}')
        private = project.get('private', True)
        request = {
            'name': name, 'description': description, 'private': private
        }
        resp = requests.post(
            f'{url}/api/v1/admin/users/{owner}/repos',
            json=request,
            auth=auth)
        if resp.status_code not in [200, 201]:
            print(resp.content.decode('utf-8'))
        for webhook in project.get('webhooks', []):
            declare_project_webhook(owner, name, webhook, token)


def configure_gogs(arg: common.RunnableParams):
    gogs = arg.conf.get('gogs_service', None)
    if gogs is None:
        print('* no configuration found')
        return
    gogs_admin_password = gogs.get('admin', None)
    if gogs_admin_password is None:
        print("Admin password is missing for GOGS")
        raise Exception('Admin password must be defined for GOGS')

    ip_adress = gogs.get('ip')

    common.render_template(
        'gogs_proxy.tmpl',
        '/etc/nginx/conf.d/proxy.conf',
        {'ip_address': ip_adress})

    key = gogs.get('key', None)
    cert = gogs.get('certificate', None)
    if key is not None and cert is not None:
        folder = '/etc/nginx/certs'
        os.mkdir(folder)
        with open(path.join(folder, 'key.pem'), 'w') as fd:
            fd.write(key)
            fd.write('\n')
        with open(path.join(folder, "cert.pem"), 'w') as fd:
            fd.write(cert)
            fd.write('\n')

    proc = subprocess.run([
        '/usr/local/bin/launch-gogs.sh',
        ip_adress,
        gogs_admin_password
    ])
    if proc.returncode != 0:
        raise Exception('Gogs configuration failed')
    while True:
        try:
            if urllib.request.urlopen(url).getcode() == 200:
                break
        except urllib.error.URLError:
            pass
        print('.', flush=True)
        time.sleep(5)
    auth = ('gogsadmin', gogs_admin_password)
    usernames = declare_users(gogs, auth)
    orgnames = declare_organizations(gogs, usernames, auth)
    declare_project(gogs, usernames, orgnames, auth)


common.register('Gogs service', 200, configure_gogs)
