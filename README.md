# gogs-service

Builds an image for a server hosting a GOGS (GO Git Server) server need for
Kanod and provide a cloud-init configuration file for the VM initialization.

Documentation is in the doc folder.
